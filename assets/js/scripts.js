var server = 'http://localhost/villa_games';
//var server = 'https://villagames.000webhostapp.com';

function gameDetail(a) {
   window.location.href = "games/game_detail/"+a;
}

function transactionDetail(id) {
   $.getJSON('transactions/transaction_detail/'+id,
   function(data){
      html = ""
      html += "<tr>"
      html += "<th>Fecha transacción</th>"
      html += "<td>"+data.transactionDetail[0].created+"</td>"
      html += "</tr>"
      html += "<tr>"
      html += "<th class='col-md-3'><p class='text-center'>Producto</p></th>"
      html += "<th class='col-md-3'><p class='text-center'>Cantidad</p></th>"
      html += "<th class='col-md-3'><p class='text-center'>Precio</p></th>"
      html += "<th class='col-md-3'><p class='text-center'>Subtotal</p></th>"
      html += "</tr>";

      for(i = 0 ; i < data.transactionDetailProducts.length; i ++) {
         html += "<tr>";
         html += "<td><p class='text-center'>"+data.transactionDetailProducts[i].name+"</p></td>";
         html += "<td><p class='text-center'>"+data.transactionDetailProducts[i].amount+"</p></td>";
         html += "<td><p class='text-center'>"+data.transactionDetailProducts[i].price+"</p></td>";
         html += "<td><p class='text-center'>"+data.transactionDetailProducts[i].subtotal+"</p></td>";
         html += "</tr>";
      }

      html += "<tr>"
      html += "<th><p class='text-center'>Total</p></th>"
      html += "<td colspan='3' align='center'>"+data.transactionDetail[0].total+"</td>"
      html += "</tr>"
      $('#products').html(html)

      $('#idTransaction').html('Detalle transacción: '+data.transactionDetail[0].id)
   })
}

function userDetail(a) {
   window.location.href = "user_edit/"+a;
}

function confirmDelete(url) {
   console.log(url)
   var r = confirm("¿Realmente deseas eliminar este elemento?")
   if (r == true) {
      window.location.replace(url)
   }
}