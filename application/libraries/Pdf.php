<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define('DOMPDF_ENABLE_AUTOLOAD', true);
require_once "./assets/dompdf/autoload.inc.php";
require_once './assets/dompdf/lib/html5lib/Parser.php';
require_once './assets/dompdf/lib/php-font-lib/src/FontLib/Autoloader.php';
require_once './assets/dompdf/lib/php-svg-lib/src/autoload.php';
require_once './assets/dompdf/src/Autoloader.php';
use Dompdf\Dompdf;

class Pdf {

  public function generate($html, $filename='', $stream=TRUE, $paper = 'A4', $orientation = "portrait")
  {
    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
    $dompdf->set_paper($paper, $orientation);
    $dompdf->render();
    if ($stream) {
        $dompdf->stream($filename.".pdf", array("Attachment" => 0));
    } else {
        return $dompdf->output();
    }
  }
}