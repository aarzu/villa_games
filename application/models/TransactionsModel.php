<?php
defined('BASEPATH') OR exist('No direct script access allowed');

class TransactionsModel extends CI_Model
{
   function __construct()
   {
      parent::__construct();
   }

   function setTransaction()
   {
      $date = date('myHi');
      $id = 'VG'.$date.rand(100,999);
      $data = array(
         'idtransaction' => $id
      );

      if ($this->db->insert('transactions', $data) ) {
         return $id;
      } else {
         return false;
      }
   }

   function getTransactions($limit, $start)
   {
      $this->db->select('tg.transaction as code,
      sum(amount) as total_productos, 
      sum(subtotal) as total_precio,
      tg.created as date');
      $this->db->limit($limit, $start);
      $this->db->order_by('tg.created DESC');
      $this->db->group_by('tg.transaction');
      $this->db->group_by('tg.created');
      $data = $this->db->get('transactions_games tg');
      return $data->result();
   }

   function getTransactionDetail($id)
   {
      # id, date created and total
      $this->db->select("t.idtransaction as id, t.created, sum(tg.subtotal) as total");
      $this->db->join('transactions_games tg','t.idtransaction = tg.transaction', 'left');
      $this->db->where('t.idtransaction', $id);
      $data = $this->db->get('transactions t');
      return $data->result();
   }

   function getTransactionDetailGames($id)
   {
      $this->db->select("g.name, tg.amount, tg.price, tg.subtotal");
      $this->db->join("transactions_games tg", "t.idtransaction = tg.transaction", "left");
      $this->db->join("games g", "tg.game = g.idgame", "left");
      $this->db->where('t.idtransaction', $id);
      $data = $this->db->get('transactions t');
      return $data->result();
   }

   function getReservations($limit, $start, $rut = false)
   {
      $this->db->select('r.idreservation as idreservation,
      r.created as date,
      r.advanced as advanced,
      r.total as total,
      r.status as status,
      g.name as game,
      g.price as price,
      c.rut as c_rut,
      c.name as c_name,
      c.last_name as c_lastname');
      $this->db->join('games g','g.idgame = r.id_game','left');

      if ($rut) {
         $this->db->like('c.rut', $rut, 'both');
      }

      $this->db->join('customers c','c.idcustomer = r.id_customer','left');
      $this->db->limit($limit, $start);
      $this->db->order_by('r.created DESC');
      $data = $this->db->get('reservations r');
      return $data->result();
   }

   function getTotalTransactions()
   {
      $this->db->select('tg.transaction as code,
      sum(amount) as total_productos, 
      sum(subtotal) as total_precio,
      tg.created as date');
      $this->db->from('transactions_games tg');
      $this->db->group_by('tg.transaction');
      $this->db->group_by('tg.created');
      return $this->db->count_all_results();
   }

   function getTotalReservations($rut = false)
   {
      $this->db->select('idreservation');
      if ($rut) {
         $this->db->join('customers c', 'c.idcustomer = reservations.id_customer');
         $this->db->like('c.rut', $rut, 'both');
      }
      $this->db->from('reservations');
      return $this->db->count_all_results();
   }

   function addGameInTransaction($transaction, $game, $amount, $price, $subtotal)
   {
      $data = array(
         'transaction' => $transaction,
         'game' => $game,
         'amount' => $amount,
         'price' => $price,
         'subtotal' => $subtotal
      );

      
      if ($this->db->insert('transactions_games', $data)) {
         return $this->db->insert_id();
      } else {
         return false;
      }
   }

   function createCustomer($customerRut, $customerName, $customerLastname, $customerPhone, $customerEmail)
   {
      $data = array(
         'rut' => $customerRut,
         'name' => $customerName,
         'last_name' => $customerLastname,
         'phone' => $customerPhone,
         'email' => $customerEmail
      );

      if ($id = $this->db->insert('customers', $data) ) {
         return $id;
      } else {
         return false;
      }
   }

   function createReservation($customer, $game, $advanced, $price, $total, $stock_reservation, $amount)
   {
      $data = array(
         'id_customer' => $customer,
         'id_game' => $game,
         'advanced' => $advanced,
         'price' => $price,
         'total' => $total
      );

      if ($id = $this->db->insert('reservations', $data) ) {
         $data_update = array(
            'stock_reservation' => $stock_reservation - $amount
         );
         $this->db->where('idgame', $game);
         $this->db->update('games', $data_update);

         return $id;
      } else {
         return false;
      }
   }

   function getCountCustomer($rut)
   {
      $this->db->select('count(*) as amount');
      $this->db->where('rut', $rut);
      $data = $this->db->get('customers');

      return $data->result();
   }

   function setCustomer($rut, $name, $last_name, $phone, $email)
   {
      $data = array(
         'rut' => $rut,
         'name' => $name,
         'last_name' => $last_name,
         'phone' => $phone,
         'email' => $email
      );

      $this->db->where('rut', $rut);
      $query = $this->db->update('customers', $data);
   }

   function getCustomerId($rut)
   {
      $this->db->select('idcustomer');
      $this->db->where('rut', $rut);
      $data = $this->db->get('customers');

      return $data->result();
   }

   function getReservationDetail($id)
   {
      $this->db->select(
         'idreservation as id,
         g.name as game_name,
         p.name as platform_name,
         r.advanced as res_advanced,
         r.idreservation,
         r.total as res_total,
         r.created as res_created,
         r.status as res_status,
         c.rut as customer_rut,
         c.name as customer_name,
         c.last_name AS customer_lastname,
         c.phone AS customer_phone,
         c.email AS customer_email'
      );
      $this->db->join('games g', 'g.idgame = r.id_game', 'left');
      $this->db->join('customers c', 'c.idcustomer = r.id_customer', 'left');
      $this->db->join('platforms p', 'p.idplatform = g.platform', 'left');

      $this->db->where('r.idreservation', $id);
      $data = $this->db->get('reservations r');
      //echo "<pre>".$this->db->last_query()."</pre>";
      return $data->result();
   }

   function updateStatusReservation($id)
   {
      $data = array('status' => 1);

      $this->db->where('idreservation', $id);
      $this->db->update('reservations', $data);
   }
}