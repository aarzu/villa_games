<?php
defined('BASEPATH') OR exist('No direct script access allowed');

class UsersModel extends CI_Model
{
   public function validateLogin($nombre, $password)
   {
      $query = $this->db->get_where('users', array('username' => $nombre));
      if($query->num_rows() == 1)
      {
         $row=$query->row();

         if(password_verify(md5($password),  password_hash($row->password,PASSWORD_DEFAULT)))
         {
            $data=array('user_data'=>array(
               'name'=>$row->name,
               'username'=>$row->username,
               'iduser'=>$row->iduser,
               'email'=>$row->email,
               'password'=>$row->password)
            );
            $this->session->set_userdata($data);
            return true;
         }
      }
      $this->session->unset_userdata('user_data');
      return false;
   }

   function getUser($user)
   {
      $this->db->where('iduser', $user);
      $data = $this->db->get('users');
      return $data->result();
   }

   function getUsers()
   {
      $this->db->select(
         '
         users.iduser,
         users.username,
         users.name,
         users.lastname,
         users.email,
         users.updated,
         users.created'
      );
      $data = $this->db->get('users');
      return $data->result();
   }

   function setUser($id, $username, $name, $lastname, $email, $password)
   {
      $data = array(
         'username' => $username,
         'name' => $name,
         'lastname' => $lastname,
         'email' => $email,
         'password' => md5($password)
      );

      $this->db->where('iduser', $id);
      $this->db->update('users', $data);
   }
}