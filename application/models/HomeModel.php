<?php
defined('BASEPATH') OR exist('No direct script access allowed');

class HomeModel extends CI_Model
{
   function __construct()
   {
      parent::__construct();
   }

   function TopSellingGames()
   {
      $this->db->select('games.name');
      $this->db->select_sum('amount');
      $this->db->join('games', 'games.idgame = transactions_games.game', 'left');
      $this->db->group_by('transactions_games.game');
      $this->db->order_by('amount', 'desc');
      $this->db->limit('10');
      $data = $this->db->get('transactions_games');

      return $data->result();
   }

   function TopCollectionGames()
   {
      $this->db->select('games.name');
      $this->db->select_sum('subtotal');
      $this->db->join('games', 'games.idgame = transactions_games.game', 'left');
      $this->db->group_by('transactions_games.game');
      $this->db->order_by('subtotal', 'desc');
      $this->db->limit('10');
      $data = $this->db->get('transactions_games');

      return $data->result();
   }

   function TotalGames()
   {
      $this->db->select('count(*) as total');
      $data = $this->db->get('games');
      
      return $data->result();
   }

   function TransactionsToday()
   {
      $this->db->select('count(*) as total');
      $this->db->where('date(created) = curdate()');
      $data = $this->db->get('transactions');
      
      return $data->result();
   }

   function ReservationsToday()
   {
      $this->db->select('count(*) as total');
      $this->db->where('date(created) = curdate()');
      $data = $this->db->get('reservations');
      
      return $data->result();
   }
}