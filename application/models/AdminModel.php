<?php
defined('BASEPATH') OR exist('No direct script access allowed');

class AdminModel extends CI_Model
{
   function __construct()
   {
      parent::__construct();
   }

   // PLATAFORMAS
   public function getPlatforms()
   {
      $data = $this->db->get('platforms');
      return $data->result();
   }

   public function setPlatform($name)
   {
      $data = array(
         'name' => $name
      );
      $this->db->insert('platforms', $data);
   }

   public function deletePlatform($platform)
   {
      if ($this->db->delete('platforms', array('idplatform' => $platform)))
      {
         return true;
      } else 
      {
         return false;
      }
   }

   // CATEGORIAS

   public function getCategories()
   {
      $data = $this->db->get('categories');
      return $data->result();
   }

   public function setCategory($name)
   {
      $data = array(
         'name' => $name
      );
      $this->db->insert('categories', $data);
   }

   public function deleteCategory($category)
   {
      if ($this->db->delete('categories', array('idcategories' => $category)))
      {
         return true;
      } else 
      {
         return false;
      }
   }

   public function getCustomers()
   {
      $data = $this->db->get('customers');
      return $data->result();
   }
}