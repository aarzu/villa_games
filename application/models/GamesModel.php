<?php
defined('BASEPATH') OR exist('No direct script access allowed');

class GamesModel extends CI_Model
{
   function __construct()
   {
      parent::__construct();
   }

   public function getGames()
   {
      $this->db->select(
         '
         games.idgame,
         games.name,
         games.price,
         games.price_cash,
         games.price_reservation,
         games.stock,
         games.stock_reservation,
         platforms.name as platform'
      );
      $this->db->join('platforms','games.platform = platforms.idplatform','left');
      $this->db->where('status', 1);
      $data = $this->db->get('games');
      return $data->result();
   }

   public function getGameDetail($game)
   {
      $this->db->where('idgame', $game);
      $data = $this->db->get('games');
      return $data->result();
   }

   public function getGameByName($game)
   {
      $this->db->select('name');
      $this->db->like('name', $game);
      $data = $this->db->get('games');
      $data = $data->result();
      return $data;
   }

   public function setGame($name, $platform, $price, $price_cash, $price_reservation ,$stock, $stock_reservation)
   {
      $data = array(
         'name' => $name,
         'platform' => $platform,
         'price' => $price,
         'price_cash' => $price_cash,
         'price_reservation' => $price_reservation,
         'stock' => $stock,
         'stock_reservation' => $stock_reservation
      );

      $this->db->insert('games', $data);

      return $this->db->insert_id();
   }

   public function setStock($game, $stock)
   {
      $data = array(
         'stock' => $stock
      );

      $this->db->where('idgame', $game);
      $this->db->update('games', $data);
   }

   public function setStockReservation($game, $stock)
   {
      $data = array(
         'stock_reservation' => $stock
      );

      $this->db->where('idgame', $game);
      $this->db->update('games', $data);
   }

   public function updateGame($game, $name, $price, $price_cash, $price_reservation, $platform)
   {
      $data = array(
         'name' => $name,
         'price' => $price,
         'price_cash' => $price_cash,
         'price_reservation' => $price_reservation,
         'platform' => $platform
      );

      $this->db->where('idgame', $game);
      $this->db->update('games', $data);
   }

   public function getPlatforms()
   {
      $data = $this->db->get('platforms');

      return $data->result();
   }

   public function delete_game($game)
   {
      $data = array('status' => 0);
      $this->db->where('idgame', $game);
      $this->db->update('games', $data);
   }
}