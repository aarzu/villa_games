<h2>Página principal</h2>

<hr>

<h4><p class="text-primary">Estadísticas generales</p></h4>
<ul>
   <li>Cantidad de juegos: <?=$totalGames[0]->total?></li>
</ul>

<hr>

<h4><p class="text-primary">Transacciones</p></h4>

<ul>
   <li>Total ventas hoy: <?=$tt[0]->total?></li>
   <li>Cantidad total preventas: <?=$rt[0]->total?></li>
</ul>

<hr>

<div class="row">
   <div class="col-md-4">
      <h4><p class="text-primary">Juegos más vendidos</p></h4>
      <ul>
         <?php for($x=0; $x < sizeof($tsg); $x++): ?>
         <li><?=$x+1?> .- <?=$tsg[$x]->name?> (<b><?=$tsg[$x]->amount?></b>)</li>
         <?php endfor; ?>
      </ul>
   </div>
   <div class="col-md-4">
      <h4><p class="text-primary">Juegos con mayor recaudación<p></h4>
      <ul>
         <?php for($x=0; $x < sizeof($tcg); $x++): ?>
         <li><?=$x+1?> .- <?=$tcg[$x]->name?> (<b>$<?=str_replace(',','.',number_format($tcg[$x]->subtotal))?></b>)</li>
         <?php endfor; ?>
      </ul>
   </div>
</div>