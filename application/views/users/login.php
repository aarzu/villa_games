<html>
<head>
<link rel="stylesheet" href="<?=base_url();?>assets/css/login.css">
      <link rel="stylesheet" href="<?=base_url();?>assets/bootstrap/css/bootstrap.css" crossorigin="anonymous">
      <link rel="stylesheet" href="<?=base_url();?>assets/bootstrap/fonts/glyphicons-halflings-regular.eot">

      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
      
   </head>
<body>
<div class="login-form">
   <form action="<?=base_url()?>user/validate" method="post">
   <div class="avatar">
      <img src="<?=base_url()?>assets/img/logo.jpg" class="img-circle img-responsive" alt="Avatar">
   </div>
      <h2 class="text-center">Login Villa-games</h2>   
      <div class="form-group">
      <input type="username" class="form-control" name="username" placeholder="Username" required="required">
      </div>
   <div class="form-group">
         <input type="password" class="form-control" name="password" placeholder="Password" required="required">
      </div>        
      <div class="form-group">
         <button type="submit" class="btn btn-primary btn-lg btn-block">Ingresar</button>
      </div>
   </form>
</div>
</body>
</html>