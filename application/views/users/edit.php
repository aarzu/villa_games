<h3>Perfil de <?php echo $user[0]->username; ?></h3>

<hr>
<form action="<?=base_url()?>user/edit/<?=$user[0]->iduser?>" method="POST" class="form-horizontal">

<div class="container col-md-10">
   <input type="hidden" name="iduser" value="<?=$user[0]->iduser?>">
   <div class="row">
      <div class="form-group col-md-4">
         <label for="inputPrice">Nombre de usuario</label>
         <input type="text" class="form-control" id="inputPrice" name="username" value="<?=$user[0]->username?>">
      </div>
   </div>
   <div class="row">
      <div class="form-group col-md-4">
         <label for="inputName">Nombre</label>
         <input type="text" class="form-control" id="inputName" name="name" value="<?=$user[0]->name?>">
      </div>
   </div>
   <div class="row">
      <div class="form-group col-md-4">
         <label for="inputName">Apellido</label>
         <input type="text" class="form-control" id="inputName" name="lastname" value="<?=$user[0]->lastname?>">
      </div>
   </div>

   <div class="row">
      <div class="form-group col-md-4">
         <label for="inputPrice">Correo</label>
         <input type="text" class="form-control" id="inputPrice" name="email" value="<?=$user[0]->email?>">
      </div>
   </div>

   <div class="row">
      <div class="form-group col-md-4">
         <label for="inputPrice">Contraseña</label>
         <input type="password" class="form-control" id="inputPrice" name="password" value="<?=$user[0]->password?>">
      </div>
   </div>
   <div class="row">
      <div class="form-group col-md-4">
         <label for="inputPrice">Repetir contraseña</label>
         <input type="password" class="form-control" id="inputPrice" name="password_repeat" value="<?=$user[0]->password?>">
      </div>
   </div>

   <div class="row">
      <div class="col-md-6" id="mensaje">
         <?php if (isset($message_error)) { ?>
            <p class="bg-danger"><?php echo $message_error; ?></p>
         <?php } ?>
         <?php if (isset($message_success)) { ?>
            <p class="bg-success"><?php echo $message_success; ?></p>
         <?php } ?>
      </div>
   </div>
</div>
<hr>

<div class="row">
   <div class="col-md-6">
      <a href="<?=base_url()?>home" class="btn btn-warning">Cancelar</a>
      <button type="submit" class="btn btn-primary">Guardar</a>
   </div>
</div>
</form>