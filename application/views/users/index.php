<h3>Lista de usuarios</h3>

<hr>

<a href="<?=base_url()?>games/new_game" class="btn btn-primary"><span class="fa fa-plus"> Nuevo usuario</span></a>

<br><br>

<table class="table table-hover">
   <thead>
      <tr>
         <th scope="col">Usuario</th>
         <th scope="col">Nombre</th>
         <th scope="col">Apellido</th>
         <th scope="col">Creación</th>
         <th></th>
      </tr>
   </thead>
   <tbody>
      <?php foreach ($users as $user): ?>
      <tr onclick="userDetail(<?=$user->iduser?>)">
         <td><?=$user->username?></td>
         <td><?=$user->name?></td>
         <td><?=$user->lastname?></td>
         <td><?=$user->created?></td>
         <td></td>
      </tr>
      <?php endforeach; ?>
   </tbody>
</table>