<html>
   <head>
      <link rel="stylesheet" href="<?=base_url();?>assets/css/styles.css">
      <link rel="stylesheet" href="<?=base_url();?>assets/bootstrap/css/bootstrap.css" crossorigin="anonymous">
      <link rel="stylesheet" href="<?=base_url();?>assets/bootstrap/fonts/glyphicons-halflings-regular.eot">

      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

      <!-- CSS file -->
      <link rel="stylesheet" href="<?=base_url();?>assets/easy_autocomplete/easy-autocomplete.min.css">

      <!-- Additional CSS Themes file - not required-->
      <link rel="stylesheet" href="<?=base_url();?>assets/easy_autocomplete/easy-autocomplete.themes.min.css">

      <link rel="shortcut icon" type="image/png" href="<?=base_url();?>assets/img/logo.ico">

      <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
      <script src="<?=base_url();?>assets/bootstrap/js/bootstrap.js"></script>
      <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
      <script src="<?=base_url();?>assets/easy_autocomplete/jquery.easy-autocomplete.min.js"></script>

      <script src="<?=base_url();?>assets/js/scripts.js" crossorigin="anonymous"></script>

      <link href="https://fonts.googleapis.com/css2?family=Lato&family=Roboto" rel="stylesheet">
      
      <title>VillaGames</title>
   </head>
   <body>