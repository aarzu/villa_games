
   <!-- MENU -->
      <div class="total-content row">
         <div class="col-sm-12 col-md-2 menu">

            <div id="header"><!-- LOGO -->
               <img src="<?=base_url()?>assets/img/logo.jpg">
            </div>

            <hr>
               <p class="message">
                  <a href='<?=base_url()?>user/edit/<?=$this->session->userdata('user_data')['iduser']?>'>
                     Bienvenido <?=ucwords(strtolower($this->session->userdata('user_data')['name']))?>
                  </a>
               </p>
            <hr>

            <div class="panel-group" id="accordion">

               <!-- HOME -->
               <div class="panel panel-default">
                  <div class="panel-heading">
                     <h4 class="panel-title">
                        <a class="item-menu" href='<?=base_url()?>home'>HOME</a>
                     </h4>
                  </div>
               </div>

               <!-- VENTAS -->
               <div class="panel panel-default">
                  <div class="panel-heading">
                     <h4 class="panel-title">
                        <a class="item-menu" data-toggle="collapse" data-parent="#accordion" href="#transactions">TRANSACCIONES</a>
                     </h4>
                  </div>
                  <div id="transactions" class="panel-collapse collapse" >
                     <div class="panel-body">
                        <a class="sub-item-menu" href="<?=base_url()?>transactions">VENTAS</a><hr>
                        <a class="sub-item-menu" href="<?=base_url()?>transactions/reservations">PREVENTAS</a>
                     </div>
                  </div>
               </div>

               <!-- PRODUCTOS -->
               <div class="panel panel-default">
                  <div class="panel-heading">
                     <h4 class="panel-title">
                        <a class="item-menu" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">PRODUCTOS</a>
                     </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse" >
                     <div class="panel-body">
                        <a class="sub-item-menu" href="<?=base_url()?>games">JUEGOS</a>
                     </div>
                  </div>
               </div>
                  
               <!-- ADMINISTRACIÓN -->
               <div class="panel panel-default">
                  <div class="panel-heading">
                     <h4 class="panel-title">
                        <a class="item-menu" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">ADMINISTRACIÓN</a>
                     </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse">
                     <div class="panel-body">
                        <a class="sub-item-menu" href="<?=base_url()?>admin/users">USUARIOS</a>
                        <a class="sub-item-menu" href="<?=base_url()?>admin/platforms">PLATAFORMAS</a>
                     </div>
                  </div>
               </div>

               <hr>

               <!-- SALIR -->
               <div class="panel panel-default">
                  <div class="panel-heading">
                     <h4 class="panel-title">
                        <a class="item-menu" href='<?=base_url()?>user/logout'><span class="glyphicon glyphicon-log-out">
                        </span> CERRAR SESIÓN</a>
                     </h4>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-10 col-sm-12 main">