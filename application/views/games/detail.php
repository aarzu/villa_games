<h3><?php echo $game[0]->name; ?></h3>

<hr>

<div class="btn-group" role="group" aria-label="...">
   <a type="button" href='<?=base_url()?>games/view_edit/<?=$game[0]->idgame?>' class="btn btn-default">
   <span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Editar</a>

   <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modalStock" aria-haspopup="true">
   <span class="glyphicon glyphicon-transfer" aria-hidden="true"></span> Stock</button>

   <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modalStockReserva" aria-haspopup="true">
   <span class="glyphicon glyphicon-transfer" aria-hidden="true"></span> Stock Reserva</button>

   <button type="button" class="btn btn-danger" onclick="confirmDelete('<?=base_url()?>games/delete_game/<?=$game[0]->idgame?>')">
   <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Eliminar</button>
</div>

<hr>
<table class="table table-bordered table-striped">
   <tr>
      <th>Nombre</th>
      <td><?php echo $game[0]->name; ?></td>
   </tr>
   <tr>
      <th>Precio tarjeta:</th>
      <td>$<?php echo str_replace(',','.',number_format($game[0]->price)); ?></td>
   </tr>
   <tr>
      <th>Precio efectivo:</th>
      <td>$<?php echo str_replace(',','.',number_format($game[0]->price_cash)); ?></td>
   </tr>
   <tr>
      <th>Precio reserva:</th>
      <td>$<?php echo str_replace(',','.',number_format($game[0]->price_reservation)); ?></td>
   </tr>
   <tr>
      <th>Stock:</th>
      <td><?php echo $game[0]->stock; ?></td>
   </tr>
   <tr>
      <th>Stock de reseva:</th>
      <td><?php echo $game[0]->stock_reservation; ?></td>
   </tr>
   <tr>
      <th>Actualizado:</th>
      <td><?php echo $game[0]->updated; ?></td>
   </tr>
   <tr>
      <th>Creado:</th>
      <td><?php echo $game[0]->created; ?></td>
   </tr>
</table>

<hr>

<div class="row">
   <div class="col-md-2">
      <a href="<?=base_url()?>games" class="btn btn-warning">Lista de juegos</a>
   </div>
   <div class="col-md-2"></div>
</div>

<!-- MODAL STOCK -->
<div class="modal fade" id="modalStock" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title" id="exampleModalLabel">Editar stock</h4>
         </div>
         <form action='<?=base_url()?>games/update_stock/<?=$game[0]->idgame?>' method="POST">
         <div class="modal-body">
            <div class="form-group">
               <div class="radio">
                  <label><input type="radio" name="action" value="add" checked>Agregar</label>
               </div>
               <div class="radio">
                  <label><input type="radio" name="action" value="subs">Quitar</label>
               </div>
               <label for="recipient-name" class="control-label">Cantidad:</label>
               <input type="number" class="form-control" id="recipient-name" name="amount">
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary">Guardar cambios</button>
         </div>
         </form>
      </div>
   </div>
</div>

<!-- MODAL STOCK RESERVA -->
<div class="modal fade" id="modalStockReserva" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <h4 class="modal-title" id="exampleModalLabel">Editar stock de reserva</h4>
         </div>
         <form action='<?=base_url()?>games/update_stock_reservation/<?=$game[0]->idgame?>' method="POST">
         <div class="modal-body">
            <div class="form-group">
               <div class="radio">
                  <label><input type="radio" name="action" value="add" checked>Agregar</label>
               </div>
               <div class="radio">
                  <label><input type="radio" name="action" value="subs">Quitar</label>
               </div>
               <label for="recipient-name" class="control-label">Cantidad:</label>
               <input type="number" class="form-control" id="recipient-name" name="amount">
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary">Guardar cambios</button>
         </div>
         </form>
      </div>
   </div>
</div>