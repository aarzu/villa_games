<h3>Nuevo juego</h3>
<hr>
<form method="POST" action="<?=base_url()?>games/saveGame">

   <div class="row">
      <div class="form-group col-md-4">
         <label for="exampleFormControlInput1">Nombre (*): </label>
         <input type="text" class="form-control form-control-sm" name="name">
      </div>
   </div>
   <div class="row">
      <div class="form-group col-md-2">
         <label for="exampleFormControlSelect1">Plataforma (*):</label>
         <select class="form-control form-control-sm" name="platform" id="exampleFormControlSelect1">
            <option selected>-</option>
            <?php foreach ($platforms as $platform): ?>
               <option value="<?=$platform->idplatform; ?>"><?=$platform->name; ?></option>
            <?php endforeach; ?>
         </select>
      </div>
   </div>

   <div class="row">
      <div class="form-group col-md-2">
         <label for="exampleFormControlTextarea1">Precio con tarjeta:</label>
         <input type="number" class="form-control form-control-sm" rows="3" name="price">
      </div>
      <div class="form-group col-md-2">
         <label for="exampleFormControlTextarea1">Precio efectivo:</label>
         <input type="number" class="form-control form-control-sm" rows="3" name="price_cash">
      </div>
      <div class="form-group col-md-2">
         <label for="exampleFormControlTextarea1">Precio reserva:</label>
         <input type="number" class="form-control form-control-sm" rows="3" name="price_reservation">
      </div>
   </div>
   <div class="row">
      <div class="form-group col-md-1">
         <label for="exampleFormControlTextarea1">Stock (*):</label>
         <input type="number" class="form-control form-control-sm" rows="3" name="stock">
      </div>

      <div class="form-group col-md-2">
         <label for="exampleFormControlTextarea1">Stock de reserva:</label>
         <input type="number" class="form-control form-control-sm" rows="3" name="stock_reservation">
      </div>
   </div>

   <hr>

   <div class="row">
      <div class="col-md-4">
      </div>
      <div class="col-md-4">
         <a href="<?=base_url()?>games" class="btn btn-warning">Cancelar</a>
         <button type="submit" class="btn btn-primary">Registrar</button>
      </div>
   </div>
</form>