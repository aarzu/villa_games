<h3>Lista de juegos</h3>

<hr>

<a href="<?=base_url()?>games/new_game" class="btn btn-primary"><span class="fa fa-plus"> Nuevo juego</span></a>

<br><br>

<table class="table table-hover">
   <thead>
      <tr>
         <th scope="col">Nombre</th>
         <th scope="col">Plataforma</th>
         <th scope="col">Precio tarjeta</th>
         <th scope="col">Precio efectivo</th>
         <th scope="col">Stock</th>
         <th scope="col">Stock de reserva</th>
         <th></th>
      </tr>
   </thead>
   <tbody>
      <?php foreach ($games as $game): ?>
      <tr onclick="gameDetail(<?=$game->idgame?>)">
         <td><?=$game->name?></td>
         <td><?=$game->platform?></td>
         <td>$<?=str_replace(',','.',number_format($game->price))?></td>
         <td>$<?=str_replace(',','.',number_format($game->price_cash))?></td>
         <td><?=$game->stock?></td>
         <td><?=$game->stock_reservation?></td>
         <td></td>
      </tr>
      <?php endforeach; ?>
   </tbody>
</table>