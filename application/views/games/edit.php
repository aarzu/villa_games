<h3>Editar: <?php echo $game[0]->name; ?></h3>

<hr>
<form action="<?=base_url()?>games/update_game" method="POST">
   <input type="hidden" name="idgame" value="<?=$game[0]->idgame?>">

   <div class="row">
      <div class="form-group col-md-5">
         <label for="inputName">Nombre</label>
         <input type="text" class="form-control" id="inputName" name="name" value="<?=$game[0]->name?>">
      </div>
   </div>

   <div class="row">
      <div class="form-group col-md-5">
         <label for="inputPrice">Precio tarjeta</label>
         <input type="text" class="form-control" id="inputPrice" name="price" value="<?=$game[0]->price?>">
      </div>
      <div class="form-group col-md-5">
         <label for="inputPrice">Precio efectivo</label>
         <input type="text" class="form-control" id="inputPrice" name="price_cash" value="<?=$game[0]->price_cash?>">
      </div>
      <div class="form-group col-md-5">
         <label for="inputPrice">Precio reserva</label>
         <input type="text" class="form-control" id="inputPrice" name="price_reservation" value="<?=$game[0]->price_reservation?>">
      </div>
   </div>

   <div class="row">
      <div class="form-group col-md-3">
         <label>Plataforma</label>
         <select class="form-control" name="platform">
            <?php foreach ($platforms as $platform): ?>
            <option value="<?=$platform->idplatform?>" <?php if($platform->idplatform == $game[0]->platform) { echo 'selected'; } ?>><?=$platform->name?></option>
            <?php endforeach; ?>
         </select>
      </div>
   </div>

   <hr>

   <div class="row">
      <div class="col-md-2">
         <a href="<?=base_url()?>games/game_detail/<?=$game[0]->idgame?>" class="btn btn-warning">Cancelar</a>
         <button type="submit" class="btn btn-primary">Guardar</a>
      </div>
   </div>
</form>