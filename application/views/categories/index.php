<form method="POST" action="<?=base_url()?>admin/save_category">
   <div class="form-group">
      <label for="exampleFormControlInput1">Nombre</label>
      <input type="text" class="form-control form-control-sm" name="name">
   </div>
   <button type="submit" class="btn btn-primary">Registrar</button>
</form>

<br><br>

<table class="table table-hover">
   <thead>
      <tr>
         <th scope="col">Nombre</th>
         <th></th>
      </tr>
   </thead>
   <tbody>
      <?php foreach ($categories as $categorie): ?>
      <tr>
         <td><?=$categorie->name?></td>
         <td><a href="<?=base_url()?>admin/delete_category/<?=$categorie->idcategories?>" class="btn btn-default">
            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
            </a>
         </td>
      </tr>
      <?php endforeach; ?>
   </tbody>
</table>