<h3>Lista de reservas</h3>
  
<hr>

<div class="container">

   <form method="GET" action="<?=base_url()?>transactions/reservations" class="form-inline">
      <div class="form-group mx-sm-3 mb-4">
         <label for="exampleFormControlInput1" class="sr-only">Rut</label>
         <input type="text" class="form-control" name="rut" id="games" placeholder="Busqueda..." autocomplete="off">
      </div>
      <button type="submit" class="btn btn-primary mb-2">Buscar</button>
   </form>

   <hr>

   <div class="row"> <!-- BOTONES -->
      <div class="col-md-10">
         <a href="<?=base_url()?>transactions/new_reservation" class="btn btn-primary btn-lg"><span class="fa fa-plus"> Nueva reserva</span></a>
      </div>
   </div>

   <hr>
   
   <div class="row">
      <div class="col-md-9">
         <table class="table table-hover">
            <thead>
               <tr>
                  <th scope="col">Cliente</th>
                  <th scope="col">Juego</th>
                  <th scope="col">Adelanto</th>
                  <th scope="col">Total</th>
                  <th scope="col">Fecha</th>
               </tr>
            </thead>
            <tbody>
               <?php foreach($reservations as $reservation): ?>
               <tr onclick="reservation_detail('<?=$reservation->idreservation?>')" <?php if($reservation->status == 1) { echo "class='success'"; } ?>>
                  <td><?=$reservation->c_rut?></td>
                  <td><?=$reservation->game?></td>
                  <td>$<?=str_replace(',','.',number_format($reservation->advanced))?></td>
                  <td>$<?=str_replace(',','.',number_format($reservation->total))?></td>
                  <td><?=$reservation->date?></td>
               </tr>
               <?php endforeach; ?>
            </tbody>
         </table>
      </div>
      <div class="col-md-10">
         <?php if (isset($links)) { ?>
         <?php echo $links ?>
         <?php } ?>
      </div>
   </div>

</div>

<script>

   function reservation_detail(id)
   {
      window.location.replace("<?php echo base_url(); ?>transactions/reservation_detail/"+id);
   }

</script>