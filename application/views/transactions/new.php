
<div class="form-group">
   <div class="col-md-7">
   <label for="games">Nombre</label>
</div>
<div class="col-md-2">
   <label for="amount">Cantidad</label>
   <input type="number" class="form-control form-control-sm" id="gameAmount" name="amount" min="1" value="1">
</div>

<div class="col-md-9">
   <hr>
</div>

<!-- FORM -->
<div class="col-md-9 custom-form">
   <input type="text" class="form-control form-control-sm autocomplete" name="games" id="games" autocomplete="off">
   <h4>Detalle juego seleccionado</h4>
   <input id="gameId" type="hidden">
   <div class="input-group">
      <span class="input-group-addon" id="sizing-addon3">Nombre</span>
      <input id="gameName" name="gameName" class="col-md-7 form-control" aria-describedby="sizing-addon3" disabled>
   </div>
   <br>
   <div class="input-group">
      <span class="input-group-addon" id="sizing-addon3">Precio tarjeta</span>
      <input id="gamePrice" name="gamePrice" class="form-control">
   </div>
   <br>
   <div class="input-group">
      <span class="input-group-addon" id="sizing-addon3">Precio efectivo</span>
      <input id="gamePriceCash" name="gamePriceCash" class="form-control">
   </div>
   <br>
   <div class="input-group">
      <span class="input-group-addon" id="sizing-addon3">Stock</span>
      <div><input id="gameStock" name="gameStock" class="form-control" disabled></div>
   </div>
   <br>
   <div class="radio">
      <label>
         <input type="radio" name="optionPay" id="optionPay1" checked>
         Pago con tarjeta
      </label>
   </div>
   <div class="radio">
      <label>
         <input type="radio" name="optionPay" id="optionPay2">
         Pago con efectivo
      </label>
   </div>
</div>

   <div class="col-md-7"></div>
   <div class="col-md-2">
      <button type="button" class="btn btn-primary" onclick="add_game()" style="display:none;" id="btn_add">Agregar</button>
   </div>

<div class="col-md-9">
   <table class="table table-hover">
      <thead>
         <tr>
            <th scope="col">ID</th>
            <th scope="col">Nombre</th>
            <th scope="col">Precio tarjeta</th>
            <th scope="col">Precio ejectivo</th>
            <th scope="col">Precio final</th>
            <th scope="col">Cantidad</th>
            <th scope="col">Subtotal</th>
         </tr>
      </thead>
      <tbody id="gameRows"></tbody>
   </table>
</div>
<div class="col-md-5">
</div>
<div class="col-md-4">
   <a href="<?=base_url()?>transactions" class="btn btn-warning">Cancelar</a>
   <button id="confirm_transaction" type="button" class="btn btn-primary" style="display:none;">Confirmar compra</button>
</div>
<div class="col-md-9" id="message">
</div>

<script type="text/javascript">
   var options = {
   url: "<?php echo base_url(); ?>games/all",
   getValue: "name",
   list: {
      match: {
         enabled: true
      },
      onChooseEvent: function() {
         $("#btn_add").show();
			var value = $("#games").getSelectedItemData().idgame;
         $("#gameId").val(value).trigger("change");
         
         var value = $("#games").getSelectedItemData().price;
         $("#gamePrice").val(value).trigger("change");

         var value = $("#games").getSelectedItemData().price_cash;
         $("#gamePriceCash").val(value).trigger("change");
         
         var value = $("#games").getSelectedItemData().name;
         $("#gameName").val(value).trigger("change");
         
         var value = $("#games").getSelectedItemData().stock;
			$("#gameStock").val(value).trigger("change");
		}
   }
   };

   $("#games").easyAutocomplete(options);

   
   var data = Array()
   var data_general = Array()
   function add_game() {

      var games = Array()
      var html = ''
      var total = 0
      var count = 0
      var bandera = false

      games['id'] = document.getElementById("gameId").value
      games['name'] = document.getElementById("gameName").value
      games['price_card'] = document.getElementById("gamePrice").value
      games['price_cash'] = document.getElementById("gamePriceCash").value
      
      games['stock'] = document.getElementById("gameStock").value
      games['amount'] = document.getElementById("gameAmount").value

      if (document.getElementById("optionPay1").checked) {
         games['price'] = document.getElementById("gamePrice").value
      } else if (document.getElementById("optionPay2").checked) {
         games['price'] = document.getElementById("gamePriceCash").value
      }

      if (data.length > 0) { // varios items agregados
         for (var x = 0 ; x < data.length ; x ++) {
            x = data.length
            data.push(games)
            bandera = true
         }
      } else { // el primer item agregado
         if ((games['stock']*1) < (games['amount']*1)) {
            alert ('No hay stock suficiente')
         } else {
            $("#confirm_transaction").show()
            data.push(games)
            bandera = true
         }
      }

      for (var x = 0 ; x < data.length ; x ++) {
         if (games['id'] == data[x]['id']) {
            count = (count*1) + (data[x]['amount']*1)
            if (count > games['stock']) {
               if (bandera == true) {
                  data.pop()
               }
               alert ('No hay stock suficiente')
            }
         }
      }
      console.log(count)

      for (var i = 0; i < data.length ; i++) {
         html += "<tr><td>" + data[i]['id'] + "</td>"
         html += "<td>" + data[i]['name'] + "</td>"
         html += "<td>$" + data[i]['price_card'] + "</td>"
         html += "<td>$" + data[i]['price_cash'] + "</td>"
         html += "<td>$" + data[i]['price'] + "</td>"
         html += "<td>" + data[i]['amount'] + "</td>"
         html += "<td>$" + data[i]['price'] * data[i]['amount'] +"</td></tr>"

         total = ((data[i]['price'] * data[i]['amount']) + total)
      }

      html += "<hr>"
      html += "Total: $"+total
      document.getElementById("gameRows").innerHTML = html
   }

   $('#confirm_transaction').click(function() {
      var games = new Array();
      for(var i = 0; i < data.length; i++)
      {
         var collection = {
            'id' : data[i]['id'],
            'name' : data[i]['name'],
            'price' : data[i]['price'],
            'amount' : data[i]['amount']
         };
         games.push( collection );
      }
      $.ajax({ 
         type: "POST",
         url: "<?php echo base_url(); ?>transactions/process_transaction",
         data: {collection : games},
         cache: false,
         success: function()
         {
            window.location.replace("<?php echo base_url(); ?>transactions");
         }
      });
   });
</script>