<form action="<?=base_url()?>transactions/create_reservation" method="POST">
   <h3>Nueva reserva</h3>
   <hr>
   <div class="row">
      <div class="col-md-6">
         <label for="games">Seleccionar juego: </label>
         <input type="text" class="form-control form-control-sm autocomplete" name="games" id="games" autocomplete="off">
      </div>
   </div>

   <div class="row">
      <div class="col-md-2">
         <label for="amount">Cantidad</label>
         <input type="number" class="form-control form-control-sm" id="gameAmount" name="amount" min="1" value="0" disabled>
      </div>

      <div class="col-md-2">
         <label for="advanced">Anticipado</label>
         <input type="number" class="form-control form-control-sm" id="advanced" name="advanced" min="1" value="0" disabled>
      </div>
   </div>

   <div class="col-md-9">
      <hr>
   </div>

   <!-- FORM -->
   <div class="col-md-12 custom-form">

      <h4>Detalle juego seleccionado</h4>

      <input id="gameId" type="hidden" name="gameId">
      <input id="gamePrice" type="hidden" name="gamePrice">
      <input id="gameStockReservation" type="hidden" name="gameStockReservation">

      <div class="row">
         <div class="form-group col-md-6">
            <label for="gameName">Nombre:</label>
            <input type="text" class="form-control" id="gameName" disabled>
         </div>
      </div>
      
      <div class="row">
         <div class="form-group col-md-3">
            <label for="txtGamePrice">Precio:</label>
            <input type="text" class="form-control" id="txtGamePrice" disabled>
         </div>
         <div class="form-group col-md-3">
            <label for="gameStock">Stock:</label>
            <input type="text" class="form-control" id="gameStock" disabled>
         </div>
         <div class="form-group col-md-3">
            <label for="txtGameStockReservation">Stock de reserva:</label>
            <input type="text" class="form-control" id="txtGameStockReservation" disabled>
         </div>
      </div>

      <hr>

      <h4>Detalle del cliente</h4>

      <div class="row">
         <div class="form-group col-md-5">
            <label for="inputLastname">RUT (*):</label>
            <input type="text" class="form-control text-left autocomplete" id="customers" name="customerRut" placeholder="12345678-9" autocomplete="off">
         </div>
      </div>

      <div class="row">
         <div class="form-group col-md-6">
         <label for="inputLastname">Nombre:</label>
            <input type="text" class="form-control" id="inputName" name="customerName" placeholder="Juan">
         </div>

         <div class="form-group col-md-6">
            <label for="inputLastname">Apellido:</label>
            <input type="text" class="form-control" id="inputLastname" name="customerLastname" placeholder="Paredes">
         </div>
      </div>

      <div class="row">
         <div class="form-group col-md-6">
         <label for="inputLastname">Teléfono:</label>
            <input type="text" class="form-control" id="inputPhone" name="customerPhone" placeholder="123456789">
         </div>

         <div class="form-group col-md-6">
            <label for="inputLastname">Correo:</label>
            <input type="text" class="form-control" id="inputEmail" name="customerEmail" placeholder="ejemplo@correo.cl">
         </div>
      </div>

      <hr>

     <?php if (isset($error_message)) { echo "<p style='text-align:center; color:red'>".$error_message."</p><hr>"; }?>

   </div>

   <hr>

   <div class="col-md-4">
      <a href="<?=base_url()?>transactions/reservations" class="btn btn-warning">Cancelar</a>
      <button type="submit" class="btn btn-primary" id="btnConfirm" disabled>Confirmar reserva</button>
   </div>
   <div class="col-md-9" id="message">
   </div>
</form>

<script type="text/javascript">

   var validacion_cantidad = true;
   var validacion_precio = false;

   $("#advanced").keyup(function(){
      if($("#gameName").val().length > 0 ){ // valida que se haya escogido primero un producto
         if($("#advanced").val()*1 > $("#txtGamePrice").val()*1) { // valida que el adelanto no sea mayor al precio del juego
            validacion_precio = false; // no se valida
            alert("El anticipo no puede ser mayor al precio del producto.");
         } else {
            validacion_precio = true; // se valida
         }
      }
   });

   $("#gameAmount").keyup(function(){
      if($("#gameName").val().length > 0 ) { // valida que se haya escogido primero un producto
         if($("#gameAmount").val()*1 > $("#txtGameStockReservation").val()*1) {
            validacion_cantidad = false; // no se valida
            alert("La cantidad de productos no puede ser mayor al stock de reserva.");
         } else {
            validacion_cantidad = true; // se valida
         }
      }
   });

   $("#gameAmount, #advanced").keyup(function(){
      if(validacion_cantidad == true && validacion_precio == true) { // se cumplen las validaciones anteriores y se habilita el botón para confirmar la reserva
         $("#btnConfirm").prop("disabled", false);
      }else{
         $("#btnConfirm").prop("disabled", true);
      }
   });

   var options = {
   url: "<?php echo base_url(); ?>games/all",
   getValue: "name",
   list: {
      match: {
         enabled: true
      },
      onChooseEvent: function() {
         $("#btn_add").show();
			var value = $("#games").getSelectedItemData().idgame;
         $("#gameId").val(value).trigger("change");

         var value = $("#games").getSelectedItemData().price_reservation;
         $("#gamePrice").val(value).trigger("change");
         
         var value = $("#games").getSelectedItemData().price_reservation;
         $("#txtGamePrice").val(value).trigger("change");

         var value = $("#games").getSelectedItemData().price_cash;
         $("#gamePriceCash").val(value).trigger("change");
         
         var value = $("#games").getSelectedItemData().name;
         $("#gameName").val(value).trigger("change");
         
         var value = $("#games").getSelectedItemData().stock;
         $("#gameStock").val(value).trigger("change");

         var value = $("#games").getSelectedItemData().stock_reservation;
         $("#gameStockReservation").val(value).trigger("change");
         
         var value = $("#games").getSelectedItemData().stock_reservation;
         $("#txtGameStockReservation").val(value).trigger("change");
         
         if($("#txtGameStockReservation").val()*1 == 0) {
            alert("Producto sin stock de reserva.")
         } else {
            $("#gameAmount").prop('disabled', false);
            $("#gameAmount").val(1);
            $("#advanced").prop('disabled', false);
            $("#advanced").val(0);
         }
		}
   }
   };

   $("#games").easyAutocomplete(options);

   
   var data = Array()
   var data_general = Array()
   function add_game() {

      var games = Array()
      var html = ''
      var total = 0
      var count = 0
      var bandera = false

      games['id'] = document.getElementById("gameId").value
      games['name'] = document.getElementById("gameName").value
      games['price_card'] = document.getElementById("gamePrice").value
      games['price_cash'] = document.getElementById("gamePriceCash").value
      
      games['stock'] = document.getElementById("gameStock").value
      games['stock_reservation'] = document.getElementById("gameStockReservation").value
      games['amount'] = document.getElementById("gameAmount").value

      if (document.getElementById("optionPay1").checked) {
         games['price'] = document.getElementById("gamePrice").value
      } else if (document.getElementById("optionPay2").checked) {
         games['price'] = document.getElementById("gamePriceCash").value
      }

      if (data.length > 0) { // varios items agregados
         for (var x = 0 ; x < data.length ; x ++) {
            x = data.length
            data.push(games)
            bandera = true
         }
      } else { // el primer item agregado
         if ((games['stock_reservation']*1) < (games['amount']*1)) {
            alert ('No hay stock suficiente')
         } else {
            $("#confirm_transaction").show()
            data.push(games)
            bandera = true
         }
      }

      for (var x = 0 ; x < data.length ; x ++) {
         if (games['id'] == data[x]['id']) {
            count = (count*1) + (data[x]['amount']*1)
            if (count > games['stock']) {
               if (bandera == true) {
                  data.pop()
               }
               alert ('No hay stock suficiente')
            }
         }
      }
      console.log(count)

      for (var i = 0; i < data.length ; i++) {
         html += "<tr><td>" + data[i]['id'] + "</td>"
         html += "<td>" + data[i]['name'] + "</td>"
         html += "<td>$" + data[i]['price_card'] + "</td>"
         html += "<td>$" + data[i]['price_cash'] + "</td>"
         html += "<td>$" + data[i]['price'] + "</td>"
         html += "<td>" + data[i]['amount'] + "</td>"
         html += "<td>$" + data[i]['price'] * data[i]['amount'] +"</td></tr>"

         total = ((data[i]['price'] * data[i]['amount']) + total)
      }

      html += "<hr>"
      html += "Total: $"+total
      document.getElementById("gameRows").innerHTML = html
   }

   $('#confirm_transaction').click(function() {
      var games = new Array();
      for(var i = 0; i < data.length; i++)
      {
         var collection = {
            'id' : data[i]['id'],
            'name' : data[i]['name'],
            'price' : data[i]['price'],
            'amount' : data[i]['amount']
         };
         games.push( collection );
      }
      $.ajax({ 
         type: "POST",
         url: "<?php echo base_url(); ?>transactions/process_transaction",
         data: {collection : games},
         cache: false,
         success: function()
         {
            window.location.replace("<?php echo base_url(); ?>transactions");
         }
      });
   });

   var listcustomers = {
      url: "<?php echo base_url(); ?>admin/list_customers",
      getValue: "rut",
      list: {
         match: {
            enabled: true
         },
         onChooseEvent: function() {
            var value = $("#customers").getSelectedItemData().rut;
            $("#customerRut").val(value).trigger("change");
            
            var value = $("#customers").getSelectedItemData().name;
            $("#inputName").val(value).trigger("change");
            
            var value = $("#customers").getSelectedItemData().last_name;
            $("#inputLastname").val(value).trigger("change");
            
            var value = $("#customers").getSelectedItemData().phone;
            $("#inputPhone").val(value).trigger("change");
            
            var value = $("#customers").getSelectedItemData().email;
            $("#inputEmail").val(value).trigger("change");
         }
      }
   };

$("#customers").easyAutocomplete(listcustomers);
</script>