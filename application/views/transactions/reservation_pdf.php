<link rel="stylesheet" href="<?=base_url();?>assets/bootstrap/css/bootstrap.css" crossorigin="anonymous">

<style>
   table {
      font-family: Arial;
   }
</style>

<img width='100' src="./application/img/logo.jpg"><br><br>
Visítanos en calle Vicente Reyes 639 local 9 - Villarrica <br><br>

<img width="25" src="./application/img/logo_ig.png"> <img width="25" src="./application/img/logo_fb.png">  Villa-Games Chile<br>

<h3>Detalle de reserva: #<?=$reservation[0]->idreservation?></h3>
<hr>
<div class="col-md-8">

   <h4>Detalles juego</h4>
   <table class="table table-bordered">
      <tbody>
         <tr>
            <th class="col-md-3">Nombre</th>
               <td><?=$reservation[0]->game_name?></td>
         </tr>
         <tr>
            <th>Plataforma</th>
               <td><?=$reservation[0]->platform_name?></td>
         </tr>
      </tbody>
   </table>

   <h4>Detalles cliente</h4>
   <table class="table table-bordered">
      <tbody>
         <tr>
            <th class="col-md-3">RUT</th>
               <td><?=$reservation[0]->customer_rut?></td>
         </tr>
         <tr>
            <th>Nombre</th>
               <td><?=$reservation[0]->customer_name?></td>
         </tr>
         <tr>
            <th>Apellido</th>
               <td><?=$reservation[0]->customer_lastname?></td>
         </tr>
         <tr>
            <th>Teléfono</th>
               <td><?=$reservation[0]->customer_phone?></td>
         </tr>
         <tr>
            <th>Correo</th>
               <td><?=$reservation[0]->customer_email?></td>
         </tr>
      </tbody>
   </table>

   <h4>Detalles reserva</h4>
   <table class="table table-bordered">
      <tbody>
         <tr>
            <th class="col-md-3">Monto reservado</th>
               <td>$<?=str_replace(',','.',number_format($reservation[0]->res_advanced))?></td>
         </tr>
         <tr>
            <th>Monto total</th>
               <td>$<?=str_replace(',','.',number_format($reservation[0]->res_total))?></td>
         </tr>
         <tr>
            <th>Monto faltante</th>
               <td>$<?=str_replace(',','.',number_format($reservation[0]->res_total - $reservation[0]->res_advanced))?></td>
         </tr>
         <tr>
            <th>Estado</th>
               <td><?php if($reservation[0]->res_status == 0){ echo 'Pendiente'; } else { echo "Pagado"; }  ?></td>
         </tr>
         <tr>
            <th>Fecha</th>
               <td><?=$reservation[0]->res_created?></td>
         </tr>
      </tbody>
   </table>

   <p><b>IMPORTANTE: La fecha de estreno de productos en PREVENTA puede sufrir retrasos ajenos a nosotros los cuales informaremos vía correo electrónico o teléfono celular.</b></p>
</div>

<script>

function confirm_reservation(id)
{
   var r = confirm('Estas a punto de completar la reserva ¿Deseas continuar?')

   var parametros = {
      'id' : id
   }

   if (r == true) {
      $.ajax({ 
         type: "POST",
         url: "<?php echo base_url(); ?>transactions/update_reservation",
         data: parametros,
         cache: false,
         success: function()
         {
            window.location.replace("<?php echo base_url(); ?>transactions/reservation_detail/"+id);
         }
      });
   }
}
</script>