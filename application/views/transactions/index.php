<div class="container">

   <form method="POST" class="form-inline">
      <div class="form-group mx-sm-3 mb-4">
         <label for="exampleFormControlInput1" class="sr-only">Nombre</label>
         <input type="text" class="form-control" name="name" id="games" placeholder="Busqueda..." autocomplete="off">
      </div>
      <button type="submit" class="btn btn-primary mb-2">Buscar</button>
   </form>

   <hr>

   <div class="row"> <!-- BOTONES -->
      <div class="col-md-10">
         <a href="<?=base_url()?>transactions/register" class="btn btn-primary btn-lg"><span class="fa fa-plus"> Nueva venta</span></a>
      </div>
   </div>

   <hr>
   
   <div class="row">
      <div class="col-md-10">
         <table class="table table-hover">
            <thead>
               <tr>
                  <th scope="col">Código</th>
                  <th scope="col">Productos</th>
                  <th scope="col">Total</th>
                  <th scope="col">Fecha</th>
                  <th></th>
               </tr>
            </thead>
            <tbody>
               <?php foreach($transactions as $transaction): ?>
               <!--<tr data- onclick="transactionDetail('<?=$transaction->code?>')"> -->
               <tr data-target="#myModal" data-toggle="modal" onclick="transactionDetail('<?=$transaction->code?>')">
                  <td><?=$transaction->code?></td>
                  <td><?=$transaction->total_productos?></td>
                  <td>$<?=str_replace(',','.',number_format($transaction->total_precio))?></td>
                  <td><?=$transaction->date?></td>
               </tr>
               <?php endforeach; ?>
            </tbody>
         </table>
      </div>
      <div class="col-md-10">
            <?php if (isset($links)) { ?>
            <?php echo $links ?>
            <?php } ?>
      </div>
   </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="idTransaction"></h4>
      </div>
      <div class="modal-body">
      <br>
         <table class="table table-striped table-bordered">
            <tbody id="products">
            </tbody>
         </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>