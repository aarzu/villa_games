<h3>Detalle de reserva: #<?=$reservation[0]->idreservation?></h3>
<hr>
<div class="col-md-8">
   <a class="btn btn-danger" href="<?php echo base_url(); ?>transactions/reservation_detail_pdf/<?=$reservation[0]->id ?>" target="blank">Imprimir</a>
   <hr>
   <h4>Detalles juego</h4>
   <table class="table table-bordered">
      <tbody>
         <tr>
            <th class="col-md-3">Nombre</th>
               <td><?=$reservation[0]->game_name?></td>
         </tr>
         <tr>
            <th>Plataforma</th>
               <td><?=$reservation[0]->platform_name?></td>
         </tr>
      </tbody>
   </table>

   <h4>Detalles cliente</h4>
   <table class="table table-bordered">
      <tbody>
         <tr>
            <th class="col-md-3">RUT</th>
               <td><?=$reservation[0]->customer_rut?></td>
         </tr>
         <tr>
            <th>Nombre</th>
               <td><?=$reservation[0]->customer_name?></td>
         </tr>
         <tr>
            <th>Apellido</th>
               <td><?=$reservation[0]->customer_lastname?></td>
         </tr>
         <tr>
            <th>Teléfono</th>
               <td><?=$reservation[0]->customer_phone?></td>
         </tr>
         <tr>
            <th>Correo</th>
               <td><?=$reservation[0]->customer_email?></td>
         </tr>
      </tbody>
   </table>

   <h4>Detalles reserva</h4>
   <table class="table table-bordered">
      <tbody>
         <tr>
            <th class="col-md-3">Monto reservado</th>
               <td>$<?=str_replace(',','.',number_format($reservation[0]->res_advanced))?></td>
         </tr>
         <tr>
            <th>Monto total</th>
               <td>$<?=str_replace(',','.',number_format($reservation[0]->res_total))?></td>
         </tr>
         <tr>
            <th>Monto faltante</th>
               <td>$<?=str_replace(',','.',number_format($reservation[0]->res_total - $reservation[0]->res_advanced))?></td>
         </tr>
         <tr>
            <th>Estado</th>
               <td><?php if($reservation[0]->res_status == 0){ echo 'Pendiente'; } else { echo "Pagado"; }  ?></td>
         </tr>
         <tr>
            <th>Fecha</th>
               <td><?=$reservation[0]->res_created?></td>
         </tr>
      </tbody>
   </table>

   <hr>

   <div class="row">
      <div class="col-md-12">
         <a href="<?=base_url()?>/transactions/reservations" class="col-md-3 btn btn-warning">Cancelar</a>
         <?php if ($reservation[0]->res_status == 0): ?>
         <a href="#" onclick="confirm_reservation('<?=$reservation[0]->id?>')" class="col-md-3 btn btn-primary">Confirmar</a>
         <?php endif; ?>
      </div>
   </div>
</div>

<script>

function confirm_reservation(id)
{
   var r = confirm('Estas a punto de completar la reserva ¿Deseas continuar?')

   var parametros = {
      'id' : id
   }

   if (r == true) {
      $.ajax({ 
         type: "POST",
         url: "<?php echo base_url(); ?>transactions/update_reservation",
         data: parametros,
         cache: false,
         success: function()
         {
            window.location.replace("<?php echo base_url(); ?>transactions/reservation_detail/"+id);
         }
      });
   }
}
</script>