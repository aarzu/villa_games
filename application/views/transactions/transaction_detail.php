<h3>Detalle de la transacction: #<?=$transactionDetail[0]->id?></h3>
<hr>
<div class="col-md-8">
<h4>Detalles venta</h4>
   <br>
   <table class="table table-striped table-bordered">
      <tbody>
         <tr>
            <th>Fecha transacción</th>
            <td><?=$transactionDetail[0]->created?></td>
         </tr>
         <tr>
            <th class="col-md-3"><p class="text-center">Producto</p></th>
            <th class="col-md-3"><p class="text-center">Cantidad</p></th>
            <th class="col-md-3"><p class="text-center">Precio</p></th>
            <th class="col-md-3"><p class="text-center">Subtotal</p></th>
         </tr>

         <?php foreach($transactionDetailGames as $transaction): ?>
         <tr>
            <td><?=$transaction->name?></td>
            <td><p class="text-center"><?=$transaction->amount?></p></td>
            <td><p class="text-right">$<?=str_replace(',','.',number_format($transaction->price))?></p></td>
            <td><p class="text-right">$<?=str_replace(',','.',number_format($transaction->subtotal))?></p></td>
         <?php endforeach; ?>
         <tr>
            <th><p class="text-center">Total</p></th>
            <td colspan="3" align="right">$<?=str_replace(',','.',number_format($transactionDetail[0]->total))?></td>
         </tr>
      </tbody>
   </table>