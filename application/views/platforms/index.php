<h3>Lista de plataformas</h3>
<hr>

<form method="POST" action="<?=base_url()?>admin/savePlatform" class="form-inline">
   <div class="row">
      <div class="form-group col-md-5">
         <label for="exampleFormControlInput1">Nombre</label>
         <input type="text" class="form-control form-control-sm" name="name" id="name">
      </div>
      <button type="submit" class="btn btn-primary" disabled id="btnRegister">Registrar</button>
   </div>
</form>

<hr>

<table class="table table-hover">
   <thead>
      <tr>
         <th scope="col">Nombre</th>
         <th></th>
      </tr>
   </thead>
   <tbody>
      <?php foreach ($platforms as $platform): ?>
      <tr>
         <td><?=$platform->name?></td>
         <td><a href="#" onclick="confirmDelete('<?=base_url()?>admin/delete_platform/<?=$platform->idplatform?>')" class="btn btn-default">
            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
            </a>
         </td>
      </tr>
      <?php endforeach; ?>
   </tbody>
</table>

<script type="text/javascript">
   $("#name").keyup(function() {
      if ( $("#name").val().length > 0 ) {
         $("#btnRegister").prop('disabled', false)
      } else {
         $("#btnRegister").prop('disabled', true)
      }
   });
</script>