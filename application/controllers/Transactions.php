<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions extends CI_Controller {

	function __construct()
	{
      parent::__construct();
      if (!$this->session->userdata('user_data')) {
         header('Location: '.base_url().'user/login');
      }
      $this->load->library('pagination');
      $this->load->model('GamesModel');
      $this->load->model('TransactionsModel');
   }

   public function index()
   {
      // init params
      $limit_per_page = 10;
      $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
      $total_records = $this->TransactionsModel->getTotalTransactions();

      $data["transactions"] = $this->TransactionsModel->getTransactions($limit_per_page, $start_index);

      if ($total_records > 0) 
      {
         $config['base_url'] = base_url() . 'transactions/index';
         // get current page records
         
         $config['total_rows'] = $total_records;
         $config['per_page'] = $limit_per_page;

         $config['full_tag_open'] = '<ul class="pagination pagination-lg">';
         $config['full_tag_close'] = '</ul>';

         $config['num_links'] = 10;
         $config['use_page_numbers'] = TRUE;
         $config['reuse_query_string'] = TRUE;

         $config['first_tag_open'] = '<li class="page-item">';
         $config['first_tag_close'] = '</li>';
            
         $config['next_link'] = 'Siguiente';
         $config['next_tag_open'] = '<li class="page-item">';
         $config['next_tag_close'] = '</li>';

         $config['prev_tag_open'] = '<li class="page-item">';
         $config['prev_tag_close'] = '</li>';

         $config['cur_tag_open'] = '<li class="page-item active"><a>';
         $config['cur_tag_close'] = '</a></li>';

         $config['num_tag_open'] = '<li class="page-item">';
         $config['num_tag_close'] = '</li>';

         $config['last_link'] = 'Última';
         $config['last_tag_open'] = '<li class="page-item">';
         $config['last_tag_close'] = "</li>";
         
         $this->pagination->initialize($config);
         
         // build paging links
         $data["links"] = $this->pagination->create_links();
      }
      $this->load->view('templates/header');
      $this->load->view('templates/head');
      $this->load->view('transactions/index', $data);
      $this->load->view('templates/footer');
   }

   public function register()
	{
      $this->load->view('templates/header');
		$this->load->view('templates/head');
      $this->load->view('transactions/new');
      $this->load->view('templates/footer');
   }

   function process_transaction()
   {
      $data = $this->input->post('collection');

      if ( $data ) {
         $transaction = $this->TransactionsModel->setTransaction();
         
         for ( $i = 0 ; $i < count($data) ; $i++) {
            $idgame = $data[$i]['id'];
            $amount = $data[$i]['amount'];
            $price = $data[$i]['price'];
            $subtotal = $data[$i]['amount'] * $data[$i]['price'];

            $gamedata = $this->GamesModel->getGameDetail($idgame);

            $updated_stock = $gamedata[0]->stock -$amount;

            if ($transaction) {

               $this->GamesModel->setStock($idgame, $updated_stock);

               if ($this->TransactionsModel->addGameInTransaction($transaction, $idgame, $amount, $price, $subtotal)) {
                  $this->register();
               }
            }
         }
      }
   }

   public function reservations()
   {
      $rut = $this->input->get('rut');
      // init params
      $limit_per_page = 10;
      $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

      $config;

      $data["reservations"] = $this->TransactionsModel->getReservations($limit_per_page, $start_index, $rut);

      if ($rut) {
         $total_records = $this->TransactionsModel->getTotalReservations($rut);
      } else {
         $total_records = $this->TransactionsModel->getTotalReservations();
      }

      if ($total_records > 0) 
      {
         $config['base_url'] = base_url() . 'transactions/reservations';
         // get current page records
         
         $config['total_rows'] = $total_records;
         $config['per_page'] = $limit_per_page;

         $config['full_tag_open'] = '<ul class="pagination pagination-lg">';
         $config['full_tag_close'] = '</ul>';

         $config['num_links'] = 10;
         $config['use_page_numbers'] = TRUE;
         $config['reuse_query_string'] = TRUE;

         $config['first_tag_open'] = '<li class="page-item">';
         $config['first_tag_close'] = '</li>';
            
         $config['next_link'] = 'Siguiente';
         $config['next_tag_open'] = '<li class="page-item">';
         $config['next_tag_close'] = '</li>';

         $config['prev_tag_open'] = '<li class="page-item">';
         $config['prev_tag_close'] = '</li>';

         $config['cur_tag_open'] = '<li class="page-item active"><a>';
         $config['cur_tag_close'] = '</a></li>';

         $config['num_tag_open'] = '<li class="page-item">';
         $config['num_tag_close'] = '</li>';

         $config['last_link'] = 'Última';
         $config['last_tag_open'] = '<li class="page-item">';
         $config['last_tag_close'] = "</li>";
         
         $this->pagination->initialize($config);
         
         // build paging links
         $data["links"] = $this->pagination->create_links();
      }
      $this->load->view('templates/header');
      $this->load->view('templates/head');
      $this->load->view('transactions/reservations', $data);
      $this->load->view('templates/footer');
   }

   public function new_reservation()
   {
      $this->load->view('templates/header');
      $this->load->view('templates/head');
      $this->load->view('transactions/new_reservation');
      $this->load->view('templates/footer');
   }

   public function create_reservation()
   {
      $customerRut = $this->input->post('customerRut');
      $customerName = $this->input->post('customerName');
      $customerLastname = $this->input->post('customerLastname');
      $customerPhone = $this->input->post('customerPhone');
      $customerEmail = $this->input->post('customerEmail');

      $game = $this->input->post('gameId');

      $price = $this->input->post('gamePrice');

      $advanced = $this->input->post('advanced');
      $amount = $this->input->post('amount');
      $stock_reservation = $this->input->post('gameStockReservation');

      $total = $price * $amount;

      $num_customer = $this->TransactionsModel->getCountCustomer($customerRut);

      if ($num_customer[0]->amount > 0) {
         $this->TransactionsModel->setCustomer($customerRut, $customerName, $customerLastname, $customerPhone, $customerEmail);
         $customer = $this->TransactionsModel->getCustomerId($customerRut);
         $customer = $customer[0]->idcustomer;
      } else if ($num_customer[0]->amount == 0) {
         $customer = $this->TransactionsModel->createCustomer($customerRut, $customerName, $customerLastname, $customerPhone, $customerEmail);
      }

      if ($customer) {
         $reservation = $this->TransactionsModel->createReservation($customer, $game, $advanced, $price, $total, $stock_reservation, $amount);

         if ($reservation) {
            header('Location: '.base_url().'transactions/reservations');
         }
      }
   }

   public function reservation_detail($id)
   {
      $data['reservation'] = $this->TransactionsModel->getReservationDetail($id);

      $this->load->view('templates/header');
      $this->load->view('templates/head');
      $this->load->view('transactions/reservation_detail', $data);
      $this->load->view('templates/footer');
   }

   public function reservation_detail_pdf($id)
   {
      $data['reservation'] = $this->TransactionsModel->getReservationDetail($id);

      $html = $this->load->view('transactions/reservation_pdf', $data, TRUE);

      // Cargamos la librería
      $this->load->library('pdf');
      // definamos un nombre para el archivo. No es necesario agregar la extension .pdf
      $filename = 'comprobante_pago';
      // generamos el PDF. Pasemos por encima de la configuración general y definamos otro tipo de papel
      $this->pdf->generate($html, $filename, true);
   }

   public function update_reservation()
   {
      $id = $this->input->post('id');

      if ($this->TransactionsModel->updateStatusReservation($id)) {
         header('Location: '.base_url().'transactions/reservation_detail/'.$id);
      }
   }

   public function transaction_detail($id)
   {
      $data['transactionDetail'] = $this->TransactionsModel->getTransactionDetail($id);
      $data['transactionDetailProducts'] = $this->TransactionsModel->getTransactionDetailGames($id);

      #formateando separador de miles para tipo de moneda CLP
      for ($i = 0 ; $i < sizeof($data['transactionDetail']); $i++){
         $data['transactionDetail'][$i]->total = "$".str_replace(',','.',number_format($data['transactionDetail'][$i]->total));
      }
      for ($i = 0 ; $i < sizeof($data['transactionDetailProducts']); $i++){
         $data['transactionDetailProducts'][$i]->price = "$".str_replace(',','.',number_format($data['transactionDetailProducts'][$i]->price));
         $data['transactionDetailProducts'][$i]->subtotal = "$".str_replace(',','.',number_format($data['transactionDetailProducts'][$i]->subtotal));
      }
      echo json_encode($data);
   }
}