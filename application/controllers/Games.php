<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Games extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('user_data')) {
			header('Location: '.base_url().'user/login');
		}
		$this->load->model('GamesModel');
	}

	public function index()
	{
		$data['in_games'] = 'in';
		$data['games'] = $this->GamesModel->getGames();
		$this->load->view('templates/header');
		$this->load->view('templates/head');
      $this->load->view('games/index', $data);
      $this->load->view('templates/footer');
	}

	public function all()
	{
		$games = $this->GamesModel->getGames();
		echo json_encode($games);
	}

	public function new_game()
	{
		$this->load->model('AdminModel');
		$data['platforms'] = $this->AdminModel->getPlatforms();

		$this->load->view('templates/header');
		$this->load->view('templates/head');
      $this->load->view('games/new', $data);
      $this->load->view('templates/footer');
	}

	public function game_detail($game)
	{
		$this->load->model('AdminModel');
		$data['game'] = $this->GamesModel->getGameDetail($game);

		$this->load->view('templates/header');
		$this->load->view('templates/head');
      $this->load->view('games/detail', $data);
      $this->load->view('templates/footer');
	}
	
	public function saveGame()
	{
		if($this->input->post()){
			$name = $this->input->post('name');
			$platform = $this->input->post('platform');
			$price = $this->input->post('price');
			$price_cash = $this->input->post('price_cash');
			$price_reservation = $this->input->post('price_reservation');
			$stock = $this->input->post('stock');
			$stock_reservation = $this->input->post('stock_reservation');

			$game = $this->GamesModel->setGame($name, $platform, $price, $price_cash, $price_reservation, $stock, $stock_reservation);

			header('Location: '.base_url().'games/game_detail/'.$game);
		}
	}

	public function update_stock($game)
	{
		$data_game = $this->GamesModel->getGameDetail($game);

		$action = $this->input->post('action');
		$amount = $this->input->post('amount');

		if ($action == 'add') {
			$result = $data_game[0]->stock + $amount;
		} else if ($action == 'subs') {
			$result = $data_game[0]->stock - $amount;
		}

		if ($result < 0) {
			echo "invalido";
		} else {
			$this->GamesModel->setStock($game, $result);
			header('Location: '.base_url().'games/game_detail/'.$game);
		}
	}

	public function update_stock_reservation($game)
	{
		$data_game = $this->GamesModel->getGameDetail($game);

		$action = $this->input->post('action');
		$amount = $this->input->post('amount');

		if ($action == 'add') {
			$result = $data_game[0]->stock_reservation + $amount;
		} else if ($action == 'subs') {
			$result = $data_game[0]->stock_reservation - $amount;
		}

		if ($result < 0) {
			echo "invalido";
		} else {
			$this->GamesModel->setStockReservation($game, $result);
			header('Location: '.base_url().'games/game_detail/'.$game);
		}
	}

	public function view_edit($game)
	{
		$this->load->model('AdminModel');
		$data['game'] = $this->GamesModel->getGameDetail($game);
		$data['platforms'] = $this->GamesModel->getPlatforms();

		$this->load->view('templates/header');
		$this->load->view('templates/head');
      $this->load->view('games/edit', $data);
      $this->load->view('templates/footer');
	}

	public function update_game()
	{
		$game = $this->input->post('idgame');
		$name = $this->input->post('name');
		$platform = $this->input->post('platform');
		$price = $this->input->post('price');
		$price_cash = $this->input->post('price_cash');
		$price_reservation = $this->input->post('price_reservation');

		$this->GamesModel->updateGame($game, $name, $price, $price_cash, $price_reservation, $platform);

		header('Location: '.base_url().'games/game_detail/'.$game);

	}

	public function delete_game($game)
	{
		$this->GamesModel->delete_game($game);

		header('Location: '.base_url().'games');
	}
}