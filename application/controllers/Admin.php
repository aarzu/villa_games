<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('user_data')) {
         header('Location: '.base_url().'user/login');
      }
		$this->load->model('AdminModel');
		$this->load->model('UsersModel');
		
	}

	public function index()
	{
		$this->load->view('templates/head');
      $this->load->view('platform/index');
      $this->load->view('templates/footer');
   }
	
	//PLATAFORMAS
	
   public function platforms()
   {
		$data['platforms'] = $this->AdminModel->getPlatforms();

		$this->load->view('templates/header');
      $this->load->view('templates/head');
      $this->load->view('platforms/index', $data);
      $this->load->view('templates/footer');
   }
	
	public function savePlatform()
	{
		if ( $this->input->post() ) {
			$name = $this->input->post('name');
         $this->AdminModel->setPlatform($name);
         
         header('Location: '.base_url().'admin/platforms');
		}
	}

	public function delete_platform($platform)
	{
		if ($this->AdminModel->deletePlatform($platform)) 
		{
			header('Location: '.base_url().'admin/platforms');
		}
	}

	// CATEGORIAS

	public function categories()
   {
		$data['categories'] = $this->AdminModel->getCategories();

      $this->load->view('templates/head');
      $this->load->view('categories/index', $data);
      $this->load->view('templates/footer');
   }

	public function save_category()
	{
		if ( $this->input->post() ) {
			$name = $this->input->post('name');
         $this->AdminModel->setCategory($name);
         header('Location: '.base_url().'admin/categories');
		}
	}

	public function delete_category($category)
	{
		if ($this->AdminModel->deleteCategory($category)) 
		{
			header('Location: '.base_url().'admin/categories');
		}
	}

	public function list_customers()
	{
		$customers = $this->AdminModel->getCustomers();
		echo json_encode($customers);
	}

	public function users()
	{
		$data['users'] = $this->UsersModel->getUsers();
		$this->load->view('templates/header');
      $this->load->view('templates/head');
      $this->load->view('users/index', $data);
      $this->load->view('templates/footer');
	}

	public function user_edit($user)
	{
		$data['user'] = $this->UsersModel->getUser($user);
		$this->load->view('templates/header');
		$this->load->view('templates/head');
      $this->load->view('users/edit', $data);
      $this->load->view('templates/footer');
	}
}