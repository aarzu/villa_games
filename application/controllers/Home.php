<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

   function __construct()
	{
      parent::__construct();
      if (!$this->session->userdata('user_data')) {
         header('Location: '.base_url().'user/login');
      }
      $this->load->model('HomeModel');
      $this->load->view('templates/header');
   }

   public function index()
	{
      $data['tsg'] = $this->HomeModel->TopSellingGames();
      $data['tcg'] = $this->HomeModel->TopCollectionGames();
      $data['totalGames'] = $this->HomeModel->TotalGames();
      $data['tt'] = $this->HomeModel->TransactionsToday();
      $data['rt'] = $this->HomeModel->ReservationsToday();

      $this->load->view('templates/head');
      $this->load->view('home', $data);
      $this->load->view('templates/footer');
   }
}