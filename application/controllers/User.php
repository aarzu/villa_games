<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

   function __construct()
	{
      parent::__construct();
      $this->load->model('UsersModel');
      $this->load->view('templates/header');
   }

   public function login()
   {
      if ($this->session->userdata('user_data')) {
			header('Location: '.base_url().'home');
      }
      $this->load->view('templates/header');
      $this->load->view('users/login');
      $this->load->view('templates/footer');
   }

   function validate()
   {
      $username = $this->input->post('username');
      $password = $this->input->post('password');
      
      $this->UsersModel->validateLogin($username, $password);

      if( $this->session->userdata('user_data') ) {
         header('Location: '.base_url().'home');
      } else {
         header('Location: '.base_url().'user/login');
      }
   }

   function logout()
   {
      $this->session->unset_userdata('user_data');
      header('Location: '.base_url().'user/login');
   }

   function register()
   {
      $this->load->view('users/new');
      $this->load->view('templates/footer');
   }

   public function edit($user)
   {
      $data['user'] = $this->UsersModel->getuser($user);

      $id = $this->input->post('iduser');
      $username = $this->input->post('username');
      $name = $this->input->post('name');
      $lastname = $this->input->post('lastname');
      $email = $this->input->post('email');
      $password = $this->input->post('password');
      $password_repeat = $this->input->post('password_repeat');

      if ($id) {
         if ($password != $password_repeat) {
            $data['message_error'] = 'Las contraseñas no coinciden.';
         } else {
            $this->UsersModel->setUser($id, $username, $name, $lastname, $email, $password);
            $data['user'] = $this->UsersModel->getuser($user);
            $data['message_success'] = 'Perfil actualizado correctamente';
         }
      }

      $this->load->view('templates/head');
      $this->load->view('users/edit', $data);
      $this->load->view('templates/footer');
   }

   function updateUser()
   {
      $id = $this->input->post('iduser');
      $username = $this->input->post('username');
      $name = $this->input->post('name');
      $lastname = $this->input->post('lastname');
      $email = $this->input->post('email');
      $password = $this->input->post('password');
      $password_repeat = $this->input->post('password_repeat');

      

      if ($password != $password_repeat) {
         $data['message'] = 'Las contraseñas no coinciden.';
         $data['message_type'] = 'error';
      } else {
         $this->UsersModel->setUser($id, $username, $name, $lastname, $email, $password);
         $data['message'] = 'Perfil actualizado correctamente';
         $data['message_type'] = 'success';
      }

   }
   
}