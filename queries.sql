select *, sum(subtotal) as total from transactions_games group by game order by total desc limit 10;

select * from transactions_games where game=2;

select * from transactions;

-- TOP 10 JUEGOS MÁS VENDIDOS
select sum(amount) as total, games.name as game from transactions_games
left join games on games.idgame = transactions_games.game
group by transactions_games.game order by total desc limit 10;

-- TOP 10 JUEGOS MAYOR REAUDACION
select sum(subtotal) as total, games.name as game from transactions_games
left join games on games.idgame = transactions_games.game
group by transactions_games.game order by total desc limit 10;

-- TRANSACCIONES DE HOY
select count(*) from transactions where date(created) = curdate();

--
ALTER TABLE `id11639443_bd_villagames`.`games` 
CHANGE COLUMN `price` `price` INT(11) NULL DEFAULT NULL ,
ADD COLUMN `price_cash` INT(11) NULL DEFAULT NULL AFTER `created`;

--
ALTER TABLE `id11639443_bd_villagames`.`games` 
CHANGE COLUMN `price_cash` `price_cash` INT(11) NULL DEFAULT NULL AFTER `price`;